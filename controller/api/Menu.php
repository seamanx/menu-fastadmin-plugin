<?php
namespace addons\sdcmenu\controller\api;

use addons\sdcmenu\model\Menu as ModelMenu;
use think\Validate;

class Menu extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $model = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelMenu();
    }

    public function index()
    {
        $where = [];
        // 默认id排序
        $orderKey = 'id';
        if($category = $this->request->request('category')){
            $where['category'] = $category;
        }
        if($order = $this->request->request('order')){
            switch($order){
                case 'score':
                    $orderKey = 'score';
                break;
                case 'view':
                    $orderKey = 'view';
                break;
            }
        }
        $menus = $this->model
            ->where($where)
            ->order($orderKey,'DESC')
            ->paginate();
        $this->success(__('Request success'),$menus);
    }

    
    public function show()
    {
        $id = $this->request->request('id');
        if(! Validate::checkRule($id,'require|number')){
            $this->error(__("Id param must required"));
        }
        $menu = $this->model->find($id);
        if(! $menu){
            $this->error(__('Not found Menu'));
        }
        $this->success(__('Request success'),$menu);
    }
}